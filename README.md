# fileogeny
Hello Rob, 
this folder includes all the files and scripts for outsourced runs.
Following these introductive words, I will tell you what to do:

## Task os2 - 18/05/2020:
You will observe the new folders

- os2\_data
- os2\_scripts

as well as two scripts os2\_step1.sh .
This time we run PhyloNetworks with up to 1 reticulation on the CFs that we obtained last week.
Thus, we use Julia. If errors occur, please let me know!
I adjusted the queue to long.q, because I saw normal.q only executes runs over two days and I guess, that will not be the case here (at least in our cluster it was not) ...

### to os2\_step1.sh:
Mainly it is a copy of its os1-pendant. It will ask for the right directory and then install required Julia-packages. 
After that again 20 jobs for each replicate will be submitted. I hope it runs without bugs - good luck!

After the runs terminate, it would be great, if you could forward it again as a zip-file... Thanks for running!

## Task os1 - 13/05/2020:
You will observe the folders

- os1\_data
- os1\_scripts
- softwares

as well as two scripts os1\_step1.sh and os1\_step2.sh (and ssh_init.sh), which are in the main path.


Please feel free to take a look into the directories. os1\_data stores all the replicates in different files. Important for the analysis are only pnw\_data.phy and pnw\_imap.txt. replicate\_data.csv stores information about the individuals of each replicate.
os1\_scripts includes all the scripts that are used in this analysis and softwares stores additional softwares which are needed - in this case only a local copy of SNPs2CF.


So how to conduct the analysis? I made two scripts that should guide you through the process from submitting runs (os1\_step1.sh) until uploading the results (os1\_step2.sh). Please run both only directly from the fileogeny directory!

### to os1\_step1.sh:
It starts with asking you for the right directory, then it would install 2 R-packages (foreach and doMC) (install\_r\_packs_snps2cf.r). In your great support, you already have these downloaded, so this step will be skipped. Then we submit for each of the 20 replicate directories the script snp2cf\_rep\_vitis.sh. This will start the analysis of pnw\_data.phy and pnw\_imap.txt in rep\_cfrepeatrrr.sh. Therefore, it refers to ms\_concordiafactoria.r and the local copy of SNPs2CF.r.

These runs will be conducted on your cluster. A folder called clusterlog will be made within the fileogeny directory and store the log-files. The output files should be stored within the respective rep_dirs.

Please let me know, if something does not behave as it should - e.g. I did not adjust the qsub preamble, but use the ISE-M one. Maybe we will need to do so, because we use a queue called long.q and I gues yours does not have the same name...

### to os1\_step2.sh:
This script should be run after the whole analysis from Step 1 is done. Again, it starts with asking you for the right directory, again it should be fileogeny. Afterwards it zips the directories clusterlog and os1\_data. Then it adds it to the gitlab repository and finally pushes it up to the cloud. Thats, where the first outsourced run will end. Thanks!
