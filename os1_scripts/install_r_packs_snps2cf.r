#!/usr/bin/env R
# 
# install_r_packs_snps2cf.r
#
# author = thomas huber
# mail = thomas.huber@evobio.eu
# 
# this script only installs packagage dependencies for SNPs2CF

install.packages('doMC', repo='http://cran.rstudio.com/')
install.packages('foreach', repo='http://cran.rstudio.com/')
print('  -> -> doMC and foreach were installed')
