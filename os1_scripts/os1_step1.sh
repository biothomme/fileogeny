#!/bin/bash
#
# this scripts starts the analysis for the first outsourced cluster-run:
# 20 replicates of a SNP-dataset of 28 Vitis ind --> SNPs2CF
# The output can be used for PhyloNetworks runs.
# 
# Please beware: only run from the downloaded directory!!!!!!

echo "--------------------------------------------------------------------"
echo "~ hello Rob, let us  start outsourced runs #1 on the INRA cluster  ~"
echo "--------------------------------------------------------------------"
echo -e "\n--> Step 1: please doulbe-check that you run it from the downloaded directory >.../fileogeny<. Otherwise, you could be facing security issues (we use chmod -R 777 for all files within the directory!).\n"

CD=`pwd`
echo -e "  >> you are in this directory:"
echo $CD
echo -e "Are you really within the directory >.../fileogeny<? Answer with Y or y.\n"
read -r q
if [ "$q" != "y" ] && [ "$q" != "Y" ]
then
        echo -e "Please change the directory to be within the downloaded directory. \nThis script terminates here ...\n\n"
        exit
fi


echo "--------------------------------------------------------------------"
echo -e "\n--> Step 2: We can proceed with our analysis. Therefore, we need to install some R-packages (foreach and doMC)...\n As you already did this step, we will skip it."

chmod -R 777 *
# R --vanilla --slave < ./os1_scripts/install_r_packs_snps2cf.r


echo "--------------------------------------------------------------------"
echo -e "\n--> Step 3: In the next turn we submit the jobs - one job per replicate. So in total we will start 20 jobs.\n"

BIGDIR='os1_data'
# BIGOUT='os1_output'
mkdir clusterlog
# mkdir $BIGOUT
DIRRO=`ls $BIGDIR | grep -e rep_`
for DIR in $DIRRO
do
    # mkdir "$BIGOUT/$DIR"
    SCRIPT="os1_scripts/snp2cf_rep_vitis.sh"
    JOB=`qsub -b y -N snpscf_samrep -cwd -l s_rt=47:58:00 -l h_rt=47:59:00 -j y $SCRIPT "$BIGDIR/$DIR" | grep -E -o '[0-9]+'`
    NUMERIC_ID="clusterlog/snps2cf_run2_"
    qalter -o ${NUMERIC_ID}${DIR}.log -j y $JOB
done


