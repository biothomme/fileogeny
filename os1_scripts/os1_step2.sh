#!/bin/bash
#
# after the cluster runs terminated, it is time to upload the data again
# therefore we use zip and git
# conduct this only from within the downloaded directory - otherwise, you
# may have security issues!

echo "--------------------------------------------------------------"
echo "~ hello Rob, let us upload results from outsourced runs #1  ~"
echo "--------------------------------------------------------------"
echo -e "\n--> Step 1: please double-check that you run it from the downloaded directory >.../fileogeny<. Otherwise, you could be facing security issues (we use chmod -R 777 for all files within the directory!).\n"

CD=`pwd`
echo -e "  >> you are in this directory:"
echo $CD
echo -e "Are you really within the directory >.../fileogeny<? Answer with Y or y.\n"
read -r q
if [ "$q" != "y" ] && [ "$q" != "Y" ]
then
        echo -e "Please change the directory to be within the downloaded directory. \nThis script terminates here ...\n\n"
        exit
fi


echo "--------------------------------------------------------------"
echo -e "\n--> Step 2: We can proceed with our upload. Therefore, we will zip the data and log folders and push them to the git repository).\n"



git pull origin master
chmod -R 777 *

DATADIR="os1_data"
ZIPFILE="os1_output.zip"
LOGDIR="clusterlog"
LOGFILE="log.zip"
zip -r $ZIPFILE $DATADIR
zip -r $LOGDIR $LOGFILE

chmod -R 777 *

git add $ZIPFILE
git add $LOGFILE

git commit -m "remote upload of processed data from INRA cluster"
git push origin master



echo "--------------------------------------------------------------"
echo "~ THANKS A LOT Rob, outsourced runs #1 is uploaded and done! ~"
echo "--------------------------------------------------------------"

