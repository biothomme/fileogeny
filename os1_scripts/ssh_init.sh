#!/bin/bash
#
# this script helps with the initiation of an ssh-key on the INRA cluster.
# an ssh-key is required for transfer of bigger amount of data using gitlab.
# please send the key to thomasmarkus.huber.3696@student.uu.se
#
# after that, I will provide ssh access to data and scripts

echo "------------------------------------------------------"
echo "~ hello Rob, let us  start to produce the ssh key ~"
echo "------------------------------------------------------"
echo -e "\n--> Step 1: please check the following line regarding that question: is the openssh version 6.5 or higher?\n" 

VERS=`ssh -V | grep -E Open`
echo -e $VERS
echo -e "Please answer with y/Y to continue: Is the openssh version 6.5 or higher?\n"
read -r q
if [ "$q" != "y" ] && [ "$q" != "Y" ]
then
	echo -e "In this case, we need to find a different solution Rob. Best would be to update OpenSSH. If that is not possible, please let me know and I will check something else. \nThis script terminates here ...\n\n"
	exit
fi

echo -e "  >> Perfect, then we can proceed!\n"

echo "------------------------------------------------------"
echo -e "\n--> Step 2: Here we will look in your home-directory for a present ssh keys of two types (ed25519, rsa; no deeper importance for us).\nplease check the following line regarding that question: do you already have an SSH key that you could share with me?\n"

if [ ! -f ~/.ssh/id_ed25519.pub ] && [ ! -f ~/.ssh/id_rsa.pub ]
then
        echo -e "  >> You do not have any ssh key so far. Let us proceed ...\n\n"
else
	if [ -f ~/.ssh/id_ed25519.pub ]
	then
		echo -e "  >> You have an SSH-file for ed25519, with the content:\n"
		head -100 ~/.ssh/id_ed25519.pub
	fi
	if [ -f ~/.ssh/id_rsa.pub ]
	then
	        echo -e "  >> You have an SSH-file for rsa, with the content:\n"
	        head -100 ~/.ssh/id_rsa.pub
	fi
	echo -e "\n  >> If you do not have a specific reason, why you do not want to share this ssh key, you can just copy and paste the line of the ssh key and forward it to me per mail.\nOtherwise press Y or y to proceed to make a new ssh key...\n\n"
	read -r q
	if [ "$q" != "y" ] && [ "$q" != "Y" ]
	then
        	echo -e "Please forward me the whole line of the ssh key (thomasmarkus.huber.3696@student.uu.se). \nThis script terminates here ...\n\n"
        	exit
	fi
fi


echo "------------------------------------------------------"
echo -e "\n--> Step 3: Next we will produce a new ssh key. It will ask you, if you want to use a passphrase. From my experience, I had a passphrase once, but it can be impeding your up- and downloads. \nTherefore, I suggest to not use a passphrase - I also stopped to do so.\nYou may ask yourself, what we are doing now: we generate an SSH key (type: ed25519) and comment it with my mail address as a reference (thomasmarkus.huber.3696@student.uu.se). This will be stored in the ssh repo within your home repository (~/.ssh/id_ed25519.pub). This is, where you could remove it again.\n  >> Just to make more precise instructions: \n   1. you will be asked for a file to save the key: DO NOT enter anything (so that the script works until the end) and press ENTER.\n   2. you will be asked for a passphrase. As described earlier: do not enter anything (as described earlier) and press ENTER. \n\n"


ssh-keygen -t ed25519 -C "thomasmarkus.huber.3696@student.uu.se"

echo -e "  >> Alright, now you should have an ssh-key! \n\n"

echo "------------------------------------------------------"

echo -e "\n--> Step 4: The last thing is to copy the ssh-key and send it to me per mail (thomasmarkus.huber.3696@student.uu.se). Please just copy the whole line:\n"

head -100 ~/.ssh/id_ed25519.pub | grep -e huber
echo -e "\n------------------------------------------------------\n"

echo -e "   ~~ End of the script, thanks Rob ~~~  "
