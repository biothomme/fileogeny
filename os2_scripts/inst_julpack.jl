#!/usr/bin/env julia
# this script is thought to install all dependecies for PhyloNetworks

using Pkg # to use functions that manage packages
Pkg.add("PhyloNetworks") # to download & install package PhyloNetworks
Pkg.add("PhyloPlots")
Pkg.add("RCall")      # packaage to call R from within julia
Pkg.add("CSV")        # to read from / write to text files, e.g. csv files
Pkg.add("DataFrames") # to create & manipulate data frames
Pkg.add("StatsModels")# for regression formulas
