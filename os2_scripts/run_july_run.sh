#!/bin/bash
# generic submission file to configure for SGE
# Beginning of SGE Options (all options begin with '#$')
# Define shell to use for this job (/bin/sh here)
#$ -S /bin/bash
# Job name
#$ -N first_phylonetworks_ret_zerone
# Using current working directory (otherwise, you will have to use '#$ wd /path/to/run')
#$ -cwd
# job time limits (h_rt is required [s_rt == software time limit / h_rt == hardware time limit])
# time limit on small.q is 10:00:00
#$ -l s_rt=239:58:00
#$ -l h_rt=239:59:00
# choose to run on a specific queue
# (qconf -sql (to list queues) qconf -sq queue_name (to print informations on this queue))
#$ -q long.q
# Redirects the standard output to the named file.
#$ -o clusterlog/pnw_rep_ret01_run1.out
##$ -e clusterlog/my_sge_test.err
# merge standard and error outputs
#$ -j y
# choose a parallel environment and run on 60 slots (use $PE_HOSTFILE)
# Export all my environment variables into job runtime context
#$ -V
# other interesting options : -t n (for n tasks array), -sync y (to wait until job is finished),
# -v PATH (to export only PATH variable))
# ...
## for more informations "man qsub"



#you could export/change some environment variables before
#export LD_LIBRARY_PATH=/usr/lib64/:$LD_LIBRARY_PATH

#JUDIR='./softwares/julia/julia'
SCRIPT='./os2_scripts/cfh.jl'
INFILE="$1/pnw_out.csv"
TREE="$1/scen_class_bl_new.new"
julia $SCRIPT $INFILE $TREE
