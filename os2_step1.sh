#!/bin/bash
#
# this scripts starts the analysis for the second outsourced cluster-run:
# Phylonetworks: inferring trees and ret1 networks from CF (obtained by SNPs2CF)
# Starting topology is scenario classique
# 
# Please beware: only run from the downloaded directory!!!!!!

echo "--------------------------------------------------------------------"
echo "~ hello Rob, let us  start outsourced runs #2 on the INRA cluster  ~"
echo "--------------------------------------------------------------------"
echo -e "\n--> Step 1: please double-check that you run it from the downloaded directory >.../fileogeny<. Otherwise, you could be facing security issues (we use chmod -R 777 for all files within the directory!).\n"

CD=`pwd`
echo -e "  >> you are in this directory:"
echo $CD
echo -e "Are you really within the directory >.../fileogeny<? Answer with Y or y.\n"
read -r q
if [ "$q" != "y" ] && [ "$q" != "Y" ]
then
        echo -e "Please change the directory to be within the downloaded directory. \nThis script terminates here ...\n\n"
        exit
fi


echo "--------------------------------------------------------------------"
echo -e "\n--> Step 2: We can proceed with our analysis. Therefore, we need to install some julia packages (PhyloNetworks, PhyloPlots, RCall, CSV, StatsModels and DataFrames )..."

chmod -R 777 *
echo -e "The requested julia packages will be installed ...\n"
julia ./os2_scripts/inst_julpack.jl


echo "--------------------------------------------------------------------"
echo -e "\n--> Step 3: In the next turn we submit the jobs - one job per replicate. So in total we will start 20 jobs.\n"

BIGDIR='os2_data'
mkdir clusterlog
DIRRO=`ls $BIGDIR | grep -e rep_`
for DIR in $DIRRO
do
    SCRIPT="os2_scripts/run_july_run.sh"
    JOB=`qsub -b y -N pnw_zerone_samrep -cwd -l s_rt=239:58:00 -l h_rt=239:59:00 -j y $SCRIPT "$BIGDIR/$DIR" | grep -E -o '[0-9]+'`
    NUMERIC_ID="clusterlog/pnw_run1_"
    qalter -o ${NUMERIC_ID}${DIR}.log -j y $JOB
done


